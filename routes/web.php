<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/customers', 'CustomerController@index')->name('customerslist');
Route::get('/users', 'UserController@index')->name('userslist');
Route::get('/products', 'ProductController@index')->name('productslist');
Route::get('/orders', 'OrderController@index')->name('orderslist');

