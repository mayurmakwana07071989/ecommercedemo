<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Products;
use App\Orders;
use App\OrderItems;
use App\Customers;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\Bouncer::allow('administrator')->toManage(User::class);

    	$admin = factory(App\User::class)->create([
        'name'      => 'administrator',
            'email'         => 'applocumadmin@yopmail.com',
            'password'      => Hash::make('Password@123')  
    ]);

    	$admin->assign('administrator');




 		
    	\Bouncer::allow('usermanager')->toManage(User::class);
    	\Bouncer::allow('usermanager')->toManage(Customers::class);

    	$usermanager = factory(App\User::class)->create([
        'name'      => 'usermanager',
            'email'         => 'usermanager@yopmail.com',
            'password'      => Hash::make('Password@123')  
    ]);

    	$usermanager->assign('usermanager');


	\Bouncer::allow('shopmanager')->toManage(Products::class);
	\Bouncer::allow('shopmanager')->toManage(Orders::class);
	\Bouncer::allow('shopmanager')->toManage(OrderItems::class);
    	$shopmanager = factory(App\User::class)->create([
        'name'      => 'shopmanager',
            'email'         => 'shopmanager@yopmail.com',
            'password'      => Hash::make('Password@123')  
    ]);

    	$shopmanager->assign('shopmanager');



        factory(Customers::class, 20)->create();
        factory(Products::class, 10)->create();
        factory(Orders::class, 5)->create();
         
    $orders = Orders::get();
    $data =array();    
    foreach ($orders as $value) {
         $product = Products::where('price',$value->total_amount)->first();
         if(isset($value->id) && isset($product->id))
         {
            OrderItems::create(['product_id'=>$product->id,'order_id'=>$value->id,'quantity'=>1]);       
         
         }
    }
        
     }
}
