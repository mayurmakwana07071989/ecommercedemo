<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Products;
use Faker\Generator as Faker;

$factory->define(Products::class, function (Faker $faker) {
	 $arrayValues = ['0', '1'];

    return [
        'name' => $faker->word,
        'price' =>$faker->randomNumber(3),
        'in_stock' => $arrayValues[rand(0,1)]
    ];
});
