<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Orders;
use App\Products;
use App\OrderItems;
use Faker\Generator as Faker;

$factory->define(Orders::class, function (Faker $faker) {
	$arrayValues = ['new', 'processed'];
	$product = Products::find(rand(1,10));
	
	//OrderItems::create(['order_id'=>$id->id,'product_id'=>$product->id,'quantity'=>1]);
	
    return [
        'total_amount' =>$product->price,
        'invoice_number' =>$faker->randomNumber(5),
        'status' => $arrayValues[rand(0,1)]
    ];
});
