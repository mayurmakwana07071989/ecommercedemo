<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;
use App\Products;


class ProductController extends Controller
{
    public function index(Request $request,Builder $builder)
    {
        
        try{
            $products_get = Products::latest();
            



            if (request()->ajax()) {
                return DataTables::of($products_get->get())
                ->addIndexColumn()

                ->editColumn('in_stock', function (Products $products) {
                	if($products->in_stock==0){
                		return 'Out of Stock';	
                	}else{
                		return 'In Stock';
                	}
                    
                })
                
                ->make(true);
            }
            $html = $builder->columns([
                ['defaultContent' => '','data' => 'DT_RowIndex','name' => 'DT_RowIndex','title' => '#','render' => null,'orderable' => false,'searchable' => false,'exportable' => false,'printable' => true,'width'=>'1%'],
                ['data' => 'name', 'name'    => 'name', 'title' => 'Product Name','width'=>'15%'],
				['data' => 'price', 'name'    => 'price', 'title' => 'Price','width'=>'15%'],
				['data' => 'in_stock', 'name'    => 'in_stock', 'title' => 'in stock','width'=>'15%'],
            ])               
            
            ->parameters([
                'order' => [],
                'processing'    => true,
                'paging'        => true,
                'info'          => true,
                'searchDelay'   => 350,
                //'dom'           => 'Bfrtip',
                //'bFilter'       => false,
                //'sDom'          => 'lfrtip',
                //'buttons'       => [ 'reset', 'reload'],
                //'searching'   => true,
                ]);

            return view('products.index',compact('html','page'));
        }catch(Exception $e){
            return redirect()->back()->with([
                'status'    => 'error',
                'title'     => 'Error!!',
                'message'   => $e->getMessage()
                ]);
        }
    }
}
