<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;
use App\Orders;

class OrderController extends Controller
{
    public function index(Request $request,Builder $builder)
    {
        
        try{
            $orders_get = Orders::join('order_items','orders.id','=','order_id')->select('invoice_number','total_amount','status','quantity','products.name as pname')->join('products','order_items.product_id','=','products.id')->orderBy('orders.id','desc');
            



            if (request()->ajax()) {
                return DataTables::of($orders_get->get())
                ->addIndexColumn()                
                ->make(true);
            }
            $html = $builder->columns([
                ['defaultContent' => '','data' => 'DT_RowIndex','name' => 'DT_RowIndex','title' => '#','render' => null,'orderable' => false,'searchable' => false,'exportable' => false,'printable' => true,'width'=>'1%'],
                ['data' => 'pname', 'name'    => 'pname', 'title' => 'Product name','width'=>'15%'],
                ['data' => 'invoice_number', 'name'    => 'invoice_number', 'title' => 'Invoice number','width'=>'15%'],
				['data' => 'total_amount', 'name'    => 'total_amount', 'title' => 'Total amount','width'=>'15%'],
				['data' => 'status', 'name'    => 'status', 'title' => 'Status','width'=>'15%'],				
            ])               
            
            ->parameters([
                'order' => [],
                'processing'    => true,
                'paging'        => true,
                'info'          => true,
                'searchDelay'   => 350,
                //'dom'           => 'Bfrtip',
                //'bFilter'       => false,
                //'sDom'          => 'lfrtip',
                //'buttons'       => [ 'reset', 'reload'],
                //'searching'   => true,
                ]);

            return view('products.index',compact('html','page'));
        }catch(Exception $e){
            return redirect()->back()->with([
                'status'    => 'error',
                'title'     => 'Error!!',
                'message'   => $e->getMessage()
                ]);
        }
    }
}
