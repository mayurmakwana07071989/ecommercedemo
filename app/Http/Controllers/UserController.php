<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;
use App\User;
class UserController extends Controller
{
    public function index(Request $request,Builder $builder)
    {
        
        try{
            $user_get = User::latest();
            



            if (request()->ajax()) {
                return DataTables::of($user_get->get())
                ->addIndexColumn()
                
                ->make(true);
            }
            $html = $builder->columns([
                ['defaultContent' => '','data' => 'DT_RowIndex','name' => 'DT_RowIndex','title' => '#','render' => null,'orderable' => false,'searchable' => false,'exportable' => false,'printable' => true,'width'=>'1%'],
                ['data' => 'name', 'name'    => 'name', 'title' => 'User Name','width'=>'15%'],
				['data' => 'email', 'name'    => 'email', 'title' => 'Email','width'=>'15%'],
            ])               
            
            ->parameters([
                'order' => [],
                'processing'    => true,
                'paging'        => true,
                'info'          => true,
                'searchDelay'   => 350,
                //'dom'           => 'Bfrtip',
                //'bFilter'       => false,
                //'sDom'          => 'lfrtip',
                //'buttons'       => [ 'reset', 'reload'],
                //'searching'   => true,
                ]);

            return view('users.index',compact('html','page'));
        }catch(Exception $e){
            return redirect()->back()->with([
                'status'    => 'error',
                'title'     => 'Error!!',
                'message'   => $e->getMessage()
                ]);
        }
    }
}
