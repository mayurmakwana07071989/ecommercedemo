<head>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="PNP THEME" name="description" />
    <meta content="pnp" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="">
    <link href="{{ asset('assets/admin/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/admin/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/admin/css/metismenu.min.css') }}" rel="stylesheet" type="text/css" />

        
    <script src="{{ asset('assets/admin/js/modernizr.min.js') }}"></script>


    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/plugins/datatables/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/plugins/datatables/responsive.bootstrap4.min.css') }}">
    
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">

                <a class="navbar-brand" href="{{ url('/') }}">
                   Ecommerce Demo
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            
<div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                 <div class="card-body">
                    @if (Auth::user()->isAn('administrator'))
                    <a href="{{ route('userslist') }}">User List</a><br/>
                    <a href="{{ route('customerslist') }}">Customer List</a><br/>
                    <a href="{{ route('productslist') }}">Product List</a><br/>
                    <a href="{{ route('orderslist') }}">Order List</a><br/>
                    @endif

                    @if (Auth::user()->isAn('usermanager'))
                    <a href="{{ route('customerslist') }}">Customer List</a><br/>
                    @endif

                    @if (Auth::user()->isAn('shopmanager'))
                     <a href="{{ route('productslist') }}">Product List</a><br/>
                     <a href="{{ route('orderslist') }}">Order List</a><br/>
                    @endif

                </div>
            </div>
        </div>
    </div>
<section class="bootstrap-checkbox" id="bootstrap-checkbox">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard">
                        {!! $html->table(['class' => 'table table-striped table-bordered'], true) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
        </main>
    </div>

     
</body>






<script src="{{ asset('assets/admin/js/jquery.min.js') }} "></script>   
<script src="{{ asset('assets/admin/js/popper.min.js') }} "></script>
<script src="{{ asset('assets/admin/js/bootstrap.min.js') }} "></script>
<script src="{{ asset('assets/admin/js/metisMenu.min.js') }} "></script>
<script src="{{ asset('assets/admin/js/waves.js') }}"></script>

<script src="{{ asset('assets/admin/js/jquery.slimscroll.js') }} "></script>
<script src="{{ asset('assets/admin/js/jquery.core.js') }} "></script>
<script src="{{ asset('assets/admin/js/jquery.app.js') }} "></script>
<script src="{{ asset('assets/admin/plugins/sweet-alert/sweetalert2.min.js') }} "></script>
<script src="{{ asset('assets/admin/pages/jquery.sweet-alert.init.js') }} "></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
    <script src="{{ asset('assets/admin/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/admin/plugins/datatables/dataTables.bootstrap4.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/admin/plugins/datatables/dataTables.responsive.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/admin/plugins/datatables/responsive.bootstrap4.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/admin/plugins/datatables/buttons.colVis.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/admin/plugins/datatables/buttons.html5.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/admin/plugins/datatables/buttons.print.min.js') }}" type="text/javascript"></script>
{!! $html->scripts() !!}

