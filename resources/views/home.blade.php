@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (Auth::user()->isAn('administrator'))
                    <a href="{{ route('userslist') }}">User List</a><br/>
                    <a href="{{ route('customerslist') }}">Customer List</a><br/>
                    <a href="{{ route('productslist') }}">Product List</a><br/>
                    <a href="{{ route('orderslist') }}">Order List</a><br/>
                    @endif

                    @if (Auth::user()->isAn('usermanager'))
                    <a href="{{ route('customerslist') }}">Customer List</a><br/>
                    @endif

                    @if (Auth::user()->isAn('shopmanager'))
                     <a href="{{ route('productslist') }}">Product List</a><br/>
                     <a href="{{ route('orderslist') }}">Order List</a><br/>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
