@if(@$page['title'] !== null)
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">{{ @$page['title'] }}</h4>
            @if(@$page['pageCrumbsLinks'] !== null)
                <ol class="breadcrumb float-right">
                    @foreach($page['pageCrumbsLinks'] as $key)
                        @if($key[0] !== null)
                            <li class="breadcrumb-item">
                                <a href="{{ $key[0] }}">{{ $key[1] }}</a>
                            </li>
                        @else
                            <li class="breadcrumb-item {{ $key[2] }}">
                                {{ $key[1] }}
                            </li>
                        @endif
                    @endforeach
                </ol>
            @endif
            @if (@$page['pageCrumbsButtons'] !== null)
                @foreach($page['pageCrumbsButtons'] as $key)
                    <a href="{{ $key[0] }}" class="{{ $key[2] }}">
                        <i class="ft-user"></i> {{ $key[1] }}</a>
                @endforeach
            @endif
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endif
