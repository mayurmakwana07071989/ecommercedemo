<script src="{{ asset('assets/admin/js/jquery.min.js') }} "></script>   
<script src="{{ asset('assets/admin/js/popper.min.js') }} "></script>
<script src="{{ asset('assets/admin/js/bootstrap.min.js') }} "></script>
<script src="{{ asset('assets/admin/js/metisMenu.min.js') }} "></script>
<script src="{{ asset('assets/admin/js/waves.js') }}"></script>

<script src="{{ asset('assets/admin/js/jquery.slimscroll.js') }} "></script>
<script src="{{ asset('assets/admin/js/jquery.core.js') }} "></script>
<script src="{{ asset('assets/admin/js/jquery.app.js') }} "></script>
<script src="{{ asset('assets/admin/plugins/sweet-alert/sweetalert2.min.js') }} "></script>
<script src="{{ asset('assets/admin/pages/jquery.sweet-alert.init.js') }} "></script>
<script>
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
</script>

@yield('scripts')

@toastr_js
    @toastr_render