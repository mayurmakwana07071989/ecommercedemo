<div class="left side-menu">
  <div class="slimscroll-menu" id="remove-scroll">
    <div id="sidebar-menu">
      <ul class="metismenu" id="side-menu">
        <li>
          <a href="{{ route('admin.dashboard') }}">
            <i class="fa fa-home"></i>
            <span> Dashboard </span>
          </a>
        </li>
        <li>
          <a href="{{ route('admin.admins.index') }}">
            <i class="fa fa-user"></i>
            <span> Admins </span>
          </a>
        </li>
        <li>
          <a href="{{ route('admin.promoters.index') }}">
            <i class="fi-briefcase"></i>
            <span> Promoters </span>
          </a>
        </li>
        <li>
          <a href="{{ route('admin.clients.index') }}">
            <i class="fa fa-users"></i>
            <span> Clients </span>
          </a>
        </li>
        <li>
          <a href="{{ route('admin.categories.index') }}">
            <i class="fi-grid"></i>
            <span> Category </span>
          </a>
        </li>
        <li>
          <a href="{{ route('admin.events.index') }}">
            <i class="fa fa-calendar"></i>
            <span> Events </span>
          </a>
        </li>
          <li>
              <a href="{{ route('admin.jobs.index') }}">
                  <i class="fa fa-list"></i>
                  <span> Jobs </span>
              </a>
          </li>
        <li>
          <a href="{{ route('admin.supports.index') }}">
           <i class="mdi mdi-account-multiple-outline"></i>
           <span> Supports </span>
         </a>
       </li>
       <li>
        <a href="{{ route('admin.broadcasts.index') }}">
          <i class="mdi mdi-bullhorn"></i>
          <span> Broadcasts </span>
        </a>
      </li>
      <li>
        <a href="{{ route('admin.scheduleds.index') }}">
          <i class="fa fa-calendar-check-o"></i>
          <span> Scheduled </span>
        </a>
      </li>
      <li>
        <a href="{{ route('settings.index') }}">
          <i class="fi-cog"></i>
          <span> Settings </span>
        </a>
      </li>
    </ul>
  </div>
  <div class="clearfix"></div>
</div>
</div>
