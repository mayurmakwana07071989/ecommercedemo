<div class="topbar">
    <div class="topbar-left">
        <a href="{{ route('admin.dashboard') }}" class="logo">
            <span>
                <img src="{{  url( \Settings::get('admin_logo_image')) }}" alt="" height="48">
            </span>
            <i>
                <img src="{{  url( \Settings::get('admin_logo_image')) }}" alt="" height="28">
            </i>
        </a>
    </div>
    <nav class="navbar-custom">
        <ul class="list-unstyled topbar-right-menu float-right mb-0">
            <li class="dropdown notification-list">
                <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                aria-haspopup="false" aria-expanded="false">
                <img src="{{ \Auth::user()->company_logo === 'default.png' ? url(\Settings::get('admin_avatar_image')) : url('storage/admins/avatar/'.Auth::user()->company_logo) }}" alt="avatar" class="rounded-circle"> <span class="ml-1">
                {{ \Auth::user()->username }} <i class="mdi mdi-chevron-down"></i> </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                <div class="dropdown-item noti-title">
                    <h6 class="text-overflow m-0">Welcome !</h6>
                </div>
                <a href="{{ route('admin.admins.myProfile',[\Auth::user()->id]) }}" class="dropdown-item notify-item">
                    <i class="fa fa fa-user-circle "></i> <span>My Profile</span>
                </a>
                <a href="{{ route('admin.logout') }}" class="dropdown-item notify-item">
                    <i class="fi-power"></i> <span>Logout</span>
                </a>
            </div>
        </li>
    </ul>
    <ul class="list-inline menu-left mb-0">
        <li class="float-left">
            <button class="button-menu-mobile open-left waves-light waves-effect">
                <i class="dripicons-menu"></i>
            </button>
        </li>
    </ul>
</nav>
</div>
